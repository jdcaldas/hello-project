'use strict';

const Koa = require('koa');

const jwt = require('koa-jwt');

const mount = require('koa-mount'),
    cors = require('koa-cors'),
    corsError = require('koa-cors-error');

const secret = require('konphyg')(__dirname + '/config')('jwt').secret;
const port = 3001;

// BUILD WITH JENKINS
const app = module.exports = Koa();

app
    .use(cors({origin: true}))
    .use(corsError)
    .use(jwt({secret: secret, passthrough: true}))
    .use(mount('/api', require('./routes')));

if (!module.parent) {
    app.listen(port, () => {
        console.log(`Server started at: ${port}`);
    });
}
