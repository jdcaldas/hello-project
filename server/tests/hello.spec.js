'use strict';

process.env.NODE_ENV = 'test';

const app = require('../index.js');
const request = require('co-supertest').agent(app.listen());
const expect = require('chai').expect;

const co = require('co');

describe('GET /api/hello', () => {
    it('should return the Hello! message', () => {
        co(function*() {
            const res = yield request.get('/api/hello').expect(200).end();
            expect(res.text).to.not.be.undefined;
            expect(res.text).to.equal("Hello World!");
        });
    });
});
