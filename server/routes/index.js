'use strict';

const Koa = require('koa'),
      app = Koa();

const Router = require('koa-joi-router'),
      Joi = Router.Joi;

const Routes = Router();

Routes.get('/hello', function*() {
    this.body = "Hello!";
});

app
.use(Routes.middleware());

module.exports = app;
